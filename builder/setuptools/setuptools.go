package setuptools

import (
	"bytes"
	"io"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/builder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/builder/pipdeptree"
)

const (
	pathPython         = "python3"
	pathPipdeptreeJSON = "pipdeptree.json"
	packageVenv        = "venv"
	nameVirtualEnv     = ".venv"
)

var _ builder.Builder = (*Builder)(nil)

// Builder generates dependency lists for setuptools projects
type Builder struct{}

// Build generates a dependency list for a setuptools script, and returns its path
func (b Builder) Build(input string) (string, []string, error) {
	dir := filepath.Dir(input)

	// install dependencies using setuptools script
	if err := installDeps(dir); err != nil {
		return "", nil, err
	}

	// save JSON output of pipdeptree
	output := filepath.Join(dir, pathPipdeptreeJSON)
	python := filepath.Join(nameVirtualEnv, "bin", pathPython)

	return output, nil, pipdeptree.CreateJSONVirtualEnv(output, python)
}

func installDeps(dir string) error {
	err := createVirtualEnvironment(dir)

	if err != nil {
		return err
	}

	if err = setupVirtualEnvironment(dir); err != nil {
		return err
	}

	return err
}

func init() {
	builder.Register("setuptools", &Builder{})
}

func createVirtualEnvironment(dir string) error {
	_, _, err := runCommand(pathPython, []string{"-m", packageVenv, nameVirtualEnv}, dir)
	return err
}

func setupVirtualEnvironment(dir string) error {
	python3 := filepath.Join("./", nameVirtualEnv, "bin", pathPython)
	_, _, err := runCommand(python3, []string{"-m", "pip", "install", "."}, dir)
	return err
}

func runCommand(command string, args []string, dir string) (io.Reader, io.Reader, error) {
	cmd := exec.Command(command, args...)
	cmd.Dir = dir
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	err := cmd.Run()
	log.Debugf("%s\n%s\n%s", cmd, &stdout, &stderr)
	return &stdout, &stderr, err
}
