package convert

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v5"
)

// NewFileConverter returns a new FileConverter
// where the package index and the dependency graph have been initialized if needed.
func NewFileConverter(f scanner.File, schemaModel uint) *FileConverter {
	index := NewIndex(f.Packages)
	graph := NewGraph(f.Packages, f.Dependencies, index)

	// build map of affected packages
	affectedMap := map[parser.Package]bool{}
	for _, aff := range f.Affections {
		affectedMap[aff.Dependency] = true
	}

	// initialize dependency converters
	dcs := []DependencyConverter{}
	for _, pkg := range f.Packages {
		dc := DependencyConverter{Package: pkg}
		dc.ShortestPath = graph.PathTo(pkg)
		dcs = append(dcs, dc)
	}

	return &FileConverter{
		File:          f,
		SchemaModel:   schemaModel,
		depConverters: dcs,
	}
}

// FileConverter is used to render a file processed by the scanner
// into a list of vulnerability objects and a dependency file object,
// to be included in the JSON report.
type FileConverter struct {
	File          scanner.File
	SchemaModel   uint
	depConverters []DependencyConverter
}

// Vulnerabilities generates a slice of vulnerability objects
// to be included in the JSON report.
func (c FileConverter) Vulnerabilities() []report.Vulnerability {
	// build a map of dependency converters
	dcMap := make(map[parser.Package]DependencyConverter)
	for _, dc := range c.depConverters {
		dcMap[dc.Package] = dc
	}

	// convert vulnerabilities
	vulns := make([]report.Vulnerability, len(c.File.Affections))
	for i, affection := range c.File.Affections {
		depConverter, found := dcMap[affection.Dependency]
		if !found {
			log.Errorf("vulnerable package %s not found in project dependencies", formatPkg(affection.Dependency))
			continue
		}
		c := VulnerabilityConverter{
			Advisory:     affection.Advisory,
			FilePath:     c.File.Path,
			DepConverter: depConverter,
			SchemaModel:  c.SchemaModel,
		}
		vulns[i] = c.Issue()
	}
	return vulns
}
