package npm

import "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/vrange/cli"

func init() {
	Register("npm")
	Register("conan")
	Register("cargo")
}

// Register registers the given resolver to use the npm resolver
func Register(resolverName string) {
	cli.Register(resolverName, "npm/rangecheck.js")
}
