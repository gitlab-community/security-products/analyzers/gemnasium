{
  "version": "15.0.4",
  "vulnerabilities": [
    {
      "id": "b8df0e45375ea52b382853cdc0184c054fff3b90a3dce33d3558c2e4db8689bb",
      "name": "SQL Injection",
      "description": "Due to an error in shallow key transformation, key and index lookups for `django.contrib.postgres.fields.JSONField`, and key lookups for `django.contrib.postgres.fields.HStoreField`, were subject to SQL injection. This could, for example, be exploited via crafted use of `OR 1=1` in a key or index name to return all records, using a suitably crafted dictionary, with dictionary expansion, as the `**kwargs` passed to the `QuerySet.filter()` function.",
      "severity": "Critical",
      "solution": "Upgrade to versions 1.11.23, 2.1.11, 2.2.4 or above.",
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-1c876f07-617f-4ba9-8ea3-a871d14bcde2",
          "value": "1c876f07-617f-4ba9-8ea3-a871d14bcde2",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-14234.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-14234",
          "value": "CVE-2019-14234",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14234"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-14234"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/aug/01/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "22636f4d8fe4a5f01bd50d9afb16fd3881601c3618bf906ba3ed1c256ca0ea47",
      "name": "Weak Password Recovery Mechanism for Forgotten Password",
      "description": "Django allows account takeover. A suitably crafted email address (that is equal to an existing user's email address after case transformation of Unicode characters) would allow an attacker to be sent a password reset token for the matched user account. (One mitigation in the new releases is to send password reset tokens only to the registered user email address.)",
      "severity": "Critical",
      "solution": "Upgrade to versions 1.11.27, 2.2.9, 3.0.1 or above.",
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-e1db52dc-7f84-4281-8b82-6a64da18a721",
          "value": "e1db52dc-7f84-4281-8b82-6a64da18a721",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-19844.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-19844",
          "value": "CVE-2019-19844",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-19844"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-19844"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/dec/18/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "f497d040a8909faa1327ef986a4c5ca364d0fb960381641c6ac621c830bbf147",
      "name": "SQL Injection",
      "description": "Django allows SQL Injection if untrusted data is used as a delimiter (e.g., in Django applications that offer downloads of data as a series of rows with a user-specified column delimiter). By passing a suitably crafted delimiter to a `contrib.postgres.aggregates.StringAgg` instance, it was possible to break escaping and inject malicious SQL.",
      "severity": "Critical",
      "solution": "Upgrade to versions 1.11.28, 2.2.10, 3.0.3 or above.",
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-ea400310-29db-4c48-8236-42d462d1de1c",
          "value": "ea400310-29db-4c48-8236-42d462d1de1c",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2020-7471.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-7471",
          "value": "CVE-2020-7471",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-7471"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/3.0/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-7471"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2020/feb/03/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "ab20db94ecf2dba6594b91c0151fa99787c409468b1606eddf116284b67d4b56",
      "name": "Information exposure in HTTP headers",
      "description": "The Requests package for Python sends an HTTP Authorization header to an HTTP URI upon receiving a same-hostname https-to-http redirect, which makes it easier for remote attackers to discover credentials by sniffing the network.",
      "severity": "Critical",
      "solution": "Upgrade to version 2.20.0 or above.",
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "requests"
          },
          "version": "2.5.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-2d0dd26c-c781-40e1-b126-7f20b21de048",
          "value": "2d0dd26c-c781-40e1-b126-7f20b21de048",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/requests/CVE-2018-18074.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2018-18074",
          "value": "CVE-2018-18074",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-18074"
        }
      ],
      "links": [
        {
          "url": "http://cwe.mitre.org/data/definitions/255.html"
        },
        {
          "url": "http://docs.python-requests.org/en/master/community/updates/#release-and-version-history"
        },
        {
          "url": "https://bugs.debian.org/910766"
        },
        {
          "url": "https://github.com/requests/requests/commit/c45d7c49ea75133e52ab22a8e9e13173938e36ff"
        },
        {
          "url": "https://github.com/requests/requests/issues/4716"
        },
        {
          "url": "https://github.com/requests/requests/pull/4718"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2018-18074"
        },
        {
          "url": "https://usn.ubuntu.com/3790-1/"
        },
        {
          "url": "https://usn.ubuntu.com/3790-2/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "requests:2.5.3"
        }
      }
    },
    {
      "id": "f0685d84348e3653ce779f8d893bb152a23f8ccaa5c3e1907d456042cef72ba1",
      "name": "Improper Input Validation",
      "description": "A call to the methods `chars()` or `words() in `django.utils.text.Truncator` with the argument `html=True` evaluates certain inputs extremely slowly due to a catastrophic backtracking vulnerability in a regular expression. The `chars()` and `words()` methods are used to implement the `truncatechars_html` and `truncatewords_html` template filters, which were thus vulnerable.",
      "severity": "High",
      "solution": "Upgrade to versions 1.11.23, 2.1.11, 2.2.4 or above.",
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-3fee86b0-d8c1-4c65-9242-7607562190ae",
          "value": "3fee86b0-d8c1-4c65-9242-7607562190ae",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-14232.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-14232",
          "value": "CVE-2019-14232",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14232"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-14232"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/aug/01/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "090638d6d6034f9b9ae6a357bade12cab6fdb0f1c65f7187134822a24379251c",
      "name": "Denial-of-service",
      "description": "If passed certain inputs, `django.utils.encoding.uri_to_iri` could lead to significant memory usage due to a recursion when repercent-encoding invalid UTF-8 octet sequences.",
      "severity": "High",
      "solution": "Upgrade to versions 1.11.23, 2.1.11, 2.2.4 or above.",
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-49afe3f9-e7ab-4d52-ad25-6e04bfdd670d",
          "value": "49afe3f9-e7ab-4d52-ad25-6e04bfdd670d",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-14235.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-14235",
          "value": "CVE-2019-14235",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14235"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-14235"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/aug/01/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "0b7c5f91e8efd5e7e0089c81e7d9a9551a402eb77048807b19efe11a6c8bf84a",
      "name": "Improper Input Validation",
      "description": "Due to the behaviour of the underlying HTMLParser, `django.utils.html.strip_tags` would be extremely slow to evaluate certain inputs containing large sequences of nested incomplete HTML entities.",
      "severity": "High",
      "solution": "Upgrade to versions 1.11.23, 2.1.11, 2.2.4 or above.",
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-4be2dbd8-301a-48cd-8ffd-db2edf367ac4",
          "value": "4be2dbd8-301a-48cd-8ffd-db2edf367ac4",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-14233.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-14233",
          "value": "CVE-2019-14233",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14233"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-14233"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/aug/01/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "9f1dbf9f3e20c0b8ec3e5682b17ebad94db30e0e535adb506e7a323403e74a68",
      "name": "SQL Injection",
      "description": "Django allows SQL Injection if untrusted data is used as a tolerance parameter in GIS functions and aggregates on Oracle. By passing a suitably crafted tolerance to GIS functions and aggregates on Oracle, it was possible to break escaping and inject malicious SQL.",
      "severity": "High",
      "solution": "Upgrade to versions 1.11.29, 2.2.11, 3.0.4 or above.",
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-5295f830-9e63-4a53-a81c-c6a7745e7aa8",
          "value": "5295f830-9e63-4a53-a81c-c6a7745e7aa8",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2020-9402.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-9402",
          "value": "CVE-2020-9402",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-9402"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/3.0/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-9402"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2020/mar/04/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "18feb275bace5b0a256e384452c7daf0ab3f6815622cc904500fd95b83de6b59",
      "name": "Uncontrolled Memory Consumption",
      "description": "Django allows Uncontrolled Memory Consumption via a malicious attacker-supplied value to the `django.utils.numberformat.format()` function.",
      "severity": "High",
      "solution": "Upgrade to versions 1.11.20, 2.0.12, 2.1.7 or above.",
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-aa6b0729-ecca-4f48-8ea0-b364044c09cc",
          "value": "aa6b0729-ecca-4f48-8ea0-b364044c09cc",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-6975.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-6975",
          "value": "CVE-2019-6975",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-6975"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/106964"
        },
        {
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-6975"
        },
        {
          "url": "https://cwe.mitre.org/data/definitions/789.html"
        },
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://groups.google.com/forum/#!topic/django-announce/WTwEAprR0IQ"
        },
        {
          "url": "https://lists.fedoraproject.org/archives/list/package-announce@lists.fedoraproject.org/message/66WMXHGBXD7GSM3PEXVCMCAGLMQYHZCU/"
        },
        {
          "url": "https://lists.fedoraproject.org/archives/list/package-announce@lists.fedoraproject.org/message/HVXDOVCXLD74SHR2BENGCE2OOYYYWJHZ/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-6975"
        },
        {
          "url": "https://usn.ubuntu.com/3890-1/"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/feb/11/security-releases/"
        },
        {
          "url": "https://www.openwall.com/lists/oss-security/2019/02/11/1"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "2accc08514e08b3560ab8fc790fb6a048e0944e2568f5829f826acad1e8011e8",
      "name": "Cross-site Scripting",
      "description": "An issue was discovered in Django. The `clickable` Current URL value displayed by the `AdminURLFieldWidget` displays the provided value without validating it as a safe URL. Thus, an unvalidated value stored in the database, or a value provided as a URL query parameter payload, could result in a clickable JavaScript link.",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.11.21, 2.1.9, 2.2.2 or above.",
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-149e1ab4-7152-4fa8-a40a-9a50a31d6d2f",
          "value": "149e1ab4-7152-4fa8-a40a-9a50a31d6d2f",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-12308.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-12308",
          "value": "CVE-2019-12308",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-12308"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/1.11.21/"
        },
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/2.1.9/"
        },
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/2.2.2/"
        },
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://github.com/django/django/commit/09186a13d975de6d049f8b3e05484f66b01ece62"
        },
        {
          "url": "https://github.com/django/django/commit/afddabf8428ddc89a332f7a78d0d21eaf2b5a673"
        },
        {
          "url": "https://github.com/django/django/commit/c238701859a52d584f349cce15d56c8e8137c52b"
        },
        {
          "url": "https://github.com/django/django/commit/deeba6d92006999fee9adfbd8be79bf0a59e8008"
        },
        {
          "url": "https://groups.google.com/forum/#!topic/django-announce/GEbHU7YoVz8"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-12308"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/jun/03/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "35d38b69aa9275e84ed2ee439efd5b3215a446e46e6298499debe07d0ff720d3",
      "name": "Incorrect Regular Expression",
      "description": "If `django.utils.text.Truncator`'s `chars()` and `words()` methods were passed the `html=True` argument, they were extremely slow to evaluate certain inputs due to a catastrophic backtracking vulnerability in a regular expression. The `chars()` and words()` methods are used to implement the `truncatechars_html` and `truncatewords_html` template filters, which were thus vulnerable.",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.8.19, 1.11.11, 2.0.3 or above.",
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-1f2c3ac1-b729-4ac9-8c64-1dcbff84250e",
          "value": "1f2c3ac1-b729-4ac9-8c64-1dcbff84250e",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2018-7537.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2018-7537",
          "value": "CVE-2018-7537",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-7537"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/103357"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2018-7537"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2018/mar/06/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "18fc9c0907e33f5697f12d120c33770da37683cf7c6cae80b1d5f3c02fa98f7b",
      "name": "URL Redirection to Untrusted Site (Open Redirect)",
      "description": "`django.middleware.common.CommonMiddleware` has an Open Redirect.",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.11.15, 2.0.8 or above.",
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-25669e1a-dcaa-4722-adfc-0f089945c95c",
          "value": "25669e1a-dcaa-4722-adfc-0f089945c95c",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2018-14574.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2018-14574",
          "value": "CVE-2018-14574",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-14574"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/104970"
        },
        {
          "url": "http://www.securitytracker.com/id/1041403"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2018-14574"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2018/aug/01/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "b4f399a9da495cc30cf138f80231217be436c4ef19961853d6c1d34d35823812",
      "name": "Improper Input Validation",
      "description": "An HTTP request is not redirected to HTTPS when the `SECURE_PROXY_SSL_HEADER` and `SECURE_SSL_REDIRECT` settings are used, and the proxy connects to Django via HTTPS. In other words, `django.http.HttpRequest.scheme` has incorrect behavior when a client uses HTTP.",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.11.22, 2.1.10, 2.2.3 or above.",
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-42367e41-2c04-4f04-bdaa-ea72d0957ef8",
          "value": "42367e41-2c04-4f04-bdaa-ea72d0957ef8",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-12781.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-12781",
          "value": "CVE-2019-12781",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-12781"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-12781"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/jul/01/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "f75b4ac54851a00e9a281833170f88339e155699c9a9fb890aac9b10b2029bf0",
      "name": "Incorrect Regular Expression",
      "description": "An issue was discovered in Django. The `django.utils.html.urlize()` function was extremely slow to evaluate certain inputs due to catastrophic backtracking vulnerabilities in two regular expressions. The `urlize()` function is used to implement the urlize and urlizetrunc template filters, which were thus vulnerable.",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.8.19, 1.11.11, 2.0.3 or above.",
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-483f1247-a680-44c4-b609-c7a3ac65bd82",
          "value": "483f1247-a680-44c4-b609-c7a3ac65bd82",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2018-7536.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2018-7536",
          "value": "CVE-2018-7536",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-7536"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/103361"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2018-7536"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2018/mar/06/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "3d413638890d3bbf7e3cc5edf7c0a8a24a504f8080100ac0f370d69709185371",
      "name": "Possible XSS in traceback section of technical 500 debug page",
      "description": "HTML auto-escaping was disabled in a portion of the template for the technical debug page. Given the right circumstances, this allowed a cross-site scripting attack. This vulnerability shouldn't affect most production sites since you shouldn't run with `DEBUG = True` (which makes this page accessible) in your production settings.",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.10.8, 1.11.5 or above.",
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-6162a015-8635-4a15-8d7c-dc9321db366f",
          "value": "6162a015-8635-4a15-8d7c-dc9321db366f",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2017-12794.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2017-12794",
          "value": "CVE-2017-12794",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12794"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/100643"
        },
        {
          "url": "http://www.securitytracker.com/id/1039264"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2017-12794"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2017/sep/05/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "57a2f29534102562dad30681a23f93c4fbc2ebbaed7400c2bfbd5132cb8ec321",
      "name": "Content Spoofing",
      "description": "Improper Neutralization of Special Elements in Output Used by a Downstream Component issue exists in `django.views.defaults.page_not_found()`, leading to content spoofing (in a 404 error page) if a user fails to recognize that a crafted URL has malicious content.",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.11.18, 2.0.10, 2.1.5 or above.",
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-94f5e552-ad49-49c7-bd9f-8857bba2354b",
          "value": "94f5e552-ad49-49c7-bd9f-8857bba2354b",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-3498.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-3498",
          "value": "CVE-2019-3498",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3498"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/106453"
        },
        {
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3498"
        },
        {
          "url": "https://cwe.mitre.org/data/definitions/148.html"
        },
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://groups.google.com/forum/#!topic/django-announce/VYU7xQQTEPQ"
        },
        {
          "url": "https://lists.debian.org/debian-lts-announce/2019/01/msg00005.html"
        },
        {
          "url": "https://lists.fedoraproject.org/archives/list/package-announce@lists.fedoraproject.org/message/HVXDOVCXLD74SHR2BENGCE2OOYYYWJHZ/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-3498"
        },
        {
          "url": "https://usn.ubuntu.com/3851-1/"
        },
        {
          "url": "https://www.debian.org/security/2019/dsa-4363"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/jan/04/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "a6be1d16f9f534936e7df625dbcf63ce13cc8a780b24730def3c364bcb07c485",
      "name": "Session fixation in resolve_redirects()",
      "description": "The `resolve_redirects()` function in `sessions.py` allows a remote, user-assisted attacker to conduct a session fixation attack. This flaw exists because the application, when establishing a new session, does not invalidate an existing session identifier and assign a new one. With a specially crafted request fixating the session identifier, a context-dependent attacker can ensure a user authenticates with the known session identifier, allowing the session to be subsequently hijacked.",
      "severity": "Medium",
      "solution": "Upgrade to latest version.",
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "requests"
          },
          "version": "2.5.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-d1c3b3de-3b82-4f45-97ce-7e97f96652e5",
          "value": "d1c3b3de-3b82-4f45-97ce-7e97f96652e5",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/requests/CVE-2015-2296.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2015-2296",
          "value": "CVE-2015-2296",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-2296"
        }
      ],
      "links": [
        {
          "url": "http://osvdb.org/show/osvdb/119576"
        },
        {
          "url": "http://www.openwall.com/lists/oss-security/2015/03/14/4"
        },
        {
          "url": "https://github.com/kennethreitz/requests/commit/3bd8afbff29e50b38f889b2f688785a669b9aafc#diff-28e67177469c0d36b068d68d9f6043bf"
        },
        {
          "url": "https://github.com/kennethreitz/requests/commit/f7c85685a8e484715649c13bacae6adc7f5f3908#diff-28e67177469c0d36b068d68d9f6043bf"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "requests:2.5.3"
        }
      }
    }
  ],
  "scan": {
    "analyzer": {
      "id": "gemnasium-python",
      "name": "gemnasium-python",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
      "vendor": {
        "name": "GitLab"
      },
      "version": "4.0.0-rc.3"
    },
    "scanner": {
      "id": "gemnasium-python",
      "name": "gemnasium-python",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
      "vendor": {
        "name": "GitLab"
      },
      "version": "4.0.0-rc.3"
    },
    "type": "dependency_scanning",
    "start_time": "2023-05-01T21:46:24",
    "end_time": "2023-05-01T21:46:42",
    "status": "success"
  }
}
