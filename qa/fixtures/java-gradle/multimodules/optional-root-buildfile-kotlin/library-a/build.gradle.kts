plugins {
    java
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":internal-module"))
    implementation("org.mozilla:rhino:1.7.10")
}
