import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "test project",
    version = "0.0.1",
    author = "Gitlab Composition Analysis",
    author_email = "sca@gitlab.com",
    description = ("A project for testing setuptools support in "
                                   "Dependency Scanning"),
    license = "BSD",
    keywords = "Dependency Scanning test project",
    url = "https://gitlab.com/gitlab-org/security-products/tests/python-setuptools/",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
    install_requires=[
        "docutils==0.13.1",
        "Django==1.11.3",
        "requests==2.5.3",
        "certifi>=2024.07.04" 
    ]
)
