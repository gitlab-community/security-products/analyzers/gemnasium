package scanner

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"sort"
	"strings"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"

	"github.com/stretchr/testify/require"
	"github.com/urfave/cli/v2"

	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/cargo"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/cocoapods"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/gemfile"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/golang"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/pipdeptree"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/swift"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/yarn"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/finder"
	vrange "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/vrange/cli"

	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/vrange/golang"
)

const fixturesDir = "testdata/depfiles"

func init() {
	// register version range resolvers
	for _, syntax := range []string{"npm", "gem", "python", "swift", "cocoapods", "cargo"} {
		vrange.Register(syntax, "../vrange/semver/vrange-"+runtime.GOOS, "npm")
	}
}

func TestScanner(t *testing.T) {
	// scanner
	scanner, err := createTestScanner("testdata/gemnasium-db")
	require.NoError(t, err)

	t.Run("ScanProjects", func(t *testing.T) {
		projects := []finder.Project{
			{
				Dir: "pypi",
				Files: []finder.File{
					{
						Filename: "pipdeptree.json",
						FileType: finder.FileTypeGraphExport,
					},
				},
				PackageManager: finder.PackageManagerPip,
			},
			{
				Dir: "go",
				Files: []finder.File{
					{
						Filename: "go.sum",
						FileType: finder.FileTypeLockFile,
					},
				},
				PackageManager: finder.PackageManagerGo,
			},
			{
				Dir: "yarn",
				Files: []finder.File{
					{
						Filename: "yarn.lock",
						FileType: finder.FileTypeLockFile,
					},
				},
				PackageManager: finder.PackageManagerYarn,
			},
			{
				Dir: "bundler",
				Files: []finder.File{
					{
						Filename: "Gemfile.lock",
						FileType: finder.FileTypeLockFile,
					},
				},
				PackageManager: finder.PackageManagerBundler,
			},
		}
		got, err := scanner.ScanProjects(fixturesDir, projects)
		require.NoError(t, err)

		// ignore dependencies because pointers cannot be compared
		for i := range got {
			got[i].Dependencies = nil
		}

		want := []File{}
		sort.Slice(got, func(i, j int) bool {
			return got[j].Path > got[i].Path
		})
		checkExpectedFile(t, &want, &got, filepath.Join(fixturesDir, "scandir.json"))
	})

	t.Run("scanFile", func(t *testing.T) {
		t.Run("Gemfile.lock", func(t *testing.T) {
			relPath, alias := "bundler/Gemfile.lock", "bundler/Gemfile"
			path := filepath.Join(fixturesDir, relPath)
			got := &File{Path: alias, PackageManager: "bundler"}

			err := scanner.scanFile(path, got)
			require.NoError(t, err)
			want := File{}
			checkExpectedFile(t, &want, got, filepath.Join(fixturesDir, "scanfile-gem.json"))
		})

		t.Run("pipdeptree.json", func(t *testing.T) {
			relPath := "pypi/pipdeptree.json"
			path := filepath.Join(fixturesDir, relPath)
			got := &File{Path: relPath, PackageManager: "pip"}

			err := scanner.scanFile(path, got)
			require.NoError(t, err)
			want := File{}
			checkExpectedFile(t, &want, got, filepath.Join(fixturesDir, "scanfile-pypi.json"))
		})

		t.Run("go.sum", func(t *testing.T) {
			relPath := "go/go.sum"
			path := filepath.Join(fixturesDir, relPath)
			got := &File{Path: relPath, PackageManager: "go"}

			err := scanner.scanFile(path, got)
			require.NoError(t, err)
			want := File{}
			checkExpectedFile(t, &want, got, filepath.Join(fixturesDir, "scanfile-go.json"))
		})

		t.Run("Package.resolved", func(t *testing.T) {
			relPath := "swift/Package.resolved"
			path := filepath.Join(fixturesDir, relPath)
			got := &File{Path: relPath, PackageManager: "swift"}

			err := scanner.scanFile(path, got)
			require.NoError(t, err)
			want := File{}
			checkExpectedFile(t, &want, got, filepath.Join(fixturesDir, "scanfile-swift.json"))
		})

		t.Run("Podfile.lock", func(t *testing.T) {

			hook := setupTestLogger(t)
			relPath := "cocoapods/Podfile.lock"
			path := filepath.Join(fixturesDir, relPath)
			got := &File{Path: relPath, PackageManager: "cocoapods"}

			wantErr := skippingVulnerabilityError(got.PackageManager)

			err := scanner.scanFile(path, got)
			require.NoError(t, err)
			assertLogContains(t, hook, log.InfoLevel, wantErr.Error())

			want := File{}
			checkExpectedFile(t, &want, got, filepath.Join(fixturesDir, "scanfile-cocoapods.json"))
		})
		t.Run("Cargo.lock", func(t *testing.T) {
			hook := setupTestLogger(t)
			relPath := "cargo/Cargo.lock"
			path := filepath.Join(fixturesDir, relPath)
			got := &File{Path: relPath, PackageManager: "cargo"}

			wantErr := skippingVulnerabilityError(got.PackageManager)

			err := scanner.scanFile(path, got)
			require.NoError(t, err)
			assertLogContains(t, hook, log.InfoLevel, wantErr.Error())

			want := File{}
			checkExpectedFile(t, &want, got, filepath.Join(fixturesDir, "scanfile-cargo.json"))
		})

	})

}

func skippingVulnerabilityError(packageType string) error {
	return fmt.Errorf("skipping vulnerability checks for %s package type", packageType)
}

func assertLogContains(t *testing.T, hook *test.Hook, level log.Level, expectedMsg string) {
	t.Helper() // This marks the function as a test helper, which improves test output

	for _, entry := range hook.AllEntries() {
		if entry.Level == level && strings.Contains(entry.Message, expectedMsg) {
			return // Found the expected log entry
		}
	}

	// If we're here, we didn't find the expected log entry
	t.Errorf("Expected log entry with level %v and message %q not found", level, expectedMsg)
}

func setupTestLogger(t *testing.T) *test.Hook {
	logger, hook := test.NewNullLogger()

	// Save original logger state
	originalLogger := log.StandardLogger()

	// Replace the global logger
	log.SetOutput(logger.Out)
	log.SetLevel(logger.Level)
	log.SetFormatter(logger.Formatter)
	log.AddHook(hook)

	t.Cleanup(func() {
		// Restore original logger
		log.StandardLogger().ReplaceHooks(originalLogger.Hooks)
		log.SetOutput(originalLogger.Out)
		log.SetLevel(originalLogger.Level)
		log.SetFormatter(originalLogger.Formatter)
	})
	// Return the hook and a cleanup function
	return hook

}

// createTestScanner creates a new Scanner instance for testing purposes.
//
// It takes a dbPath string parameter representing the path to the local database.
// Option 1 is testdata/gemnasium-db-gem
// Option 2 is testdata/gemnasium-db
// It returns a pointer to a Scanner and an error.
func createTestScanner(dbPath string) (*Scanner, error) {
	set := flag.NewFlagSet("test", 0)
	set.String("gemnasium-db-local-path", dbPath, "doc")
	set.Bool("gemnasium-db-update-disabled", true, "doc")
	c := cli.NewContext(nil, set, nil)
	return NewScanner(c)
}

func TestScanner_GemOnlyRepo(t *testing.T) {
	scanner, err := createTestScanner("testdata/gemnasium-db-gem")
	require.NoError(t, err)

	tcs := []struct {
		file string
		want error
	}{
		{
			file: "pypi/pipdeptree.json",
			want: skippingVulnerabilityError("pypi"),
		},
		{
			file: "yarn/yarn.lock",
			want: skippingVulnerabilityError("npm"),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.file, func(t *testing.T) {
			hook := setupTestLogger(t)
			path := filepath.Join(fixturesDir, tc.file)
			err := scanner.scanFile(path, &File{})
			require.NoError(t, err)
			assertLogContains(t, hook, log.InfoLevel, tc.want.Error())
		})
	}

	tcs = []struct {
		file string
		want error
	}{
		{
			file: "bundler/Gemfile.lock",
			want: nil,
		},
	}
	for _, tc := range tcs {
		t.Run(tc.file, func(t *testing.T) {
			path := filepath.Join(fixturesDir, tc.file)
			got := scanner.scanFile(path, &File{})
			require.Equal(t, got, tc.want)
		})
	}
}

func checkExpectedFile(t *testing.T, want, got interface{}, path string) {
	// look for file containing expected JSON document
	if _, err := os.Stat(path); err != nil {
		createExpectedFile(t, got, path)
	} else {
		compareToExpectedFile(t, want, got, path)
	}
}

func compareToExpectedFile(t *testing.T, want, got interface{}, path string) {
	f, err := os.Open(path)
	require.NoError(t, err)
	defer f.Close()

	err = json.NewDecoder(f).Decode(want)
	require.NoError(t, err)

	require.Equal(t, want, got)
}

func createExpectedFile(t *testing.T, got interface{}, path string) {
	// make test fail
	t.Errorf("creating JSON document: %s", path)

	// create directory for file
	err := os.MkdirAll(filepath.Dir(path), 0o755)
	require.NoError(t, err)

	// create file
	f, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY, 0o644)
	require.NoError(t, err)
	defer f.Close()

	// write JSON doc
	encoder := json.NewEncoder(f)
	encoder.SetIndent("", "  ")
	encoder.SetEscapeHTML(false)
	err = encoder.Encode(got)
	require.NoError(t, err)
}
