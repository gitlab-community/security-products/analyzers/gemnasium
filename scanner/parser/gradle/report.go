package gradle

// Dependency from Gradle htmlDependencyReport task
type Dependency struct {
	Module string `json:"module"`
	Name   string `json:"name"`
	// Values are: FAILED, RESOLVED, RESOLVED_CONSTRAINT, UNRESOLVED
	// See https://github.com/gradle/gradle/blob/v8.8.0/subprojects/diagnostics/src/main/java/org/gradle/api/tasks/diagnostics/internal/graph/nodes/RenderableDependency.java#L41-L46
	//
	// FAILED signifies that the system attempted to resolve the configuration but
	// encountered an error that prevented successful resolution. This might
	// result from issues like version conflicts, missing dependencies, or network
	// errors. Essentially, FAILED reflects an active attempt that ended in
	// failure due to an external or internal problem.
	//
	// UNRESOLVED, on the other hand, indicates that no resolution attempt was
	// made because the configuration itself was not designed to be resolved. For
	// example, some configurations are meant to be used as dependency buckets or
	// are consumable by other parts of the build system, but they are not meant
	// to generate a resolved dependency graph. In these cases, Gradle doesn't
	// even try to resolve the configuration, marking it as UNRESOLVED.
	//
	// See https://gitlab.com/gitlab-org/gitlab/-/issues/482650#note_2092240910
	Resolvable      string       `json:"resolvable"`
	HasConflict     bool         `json:"hasConflict"`
	AlreadyRendered bool         `json:"alreadyRendered"`
	Children        []Dependency `json:"children"`
}

// Insight from Gradle htmlDependencyReport task
type Insight struct {
	Module  string       `json:"module"`
	Insight []Dependency `json:"insight"`
}

// Configuration from Gradle htmlDependencyReport task
type Configuration struct {
	Name           string       `json:"name"`
	Description    string       `json:"description"`
	Dependencies   []Dependency `json:"dependencies"`
	ModuleInsights []Insight    `json:"moduleInsights"`
}

// Project from Gradle htmlDependencyReport task
type Project struct {
	Name           string          `json:"name"`
	Description    string          `json:"description"`
	Configurations []Configuration `json:"configurations"`
}

// Report from Gradle htmlDependencyReport task
type Report struct {
	Version        string  `json:"gradleVersion"`
	GenerationDate string  `json:"generationDate"`
	Project        Project `json:"project"`
}
