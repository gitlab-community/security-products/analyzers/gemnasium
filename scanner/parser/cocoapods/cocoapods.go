package cocoapods

import (
	"fmt"
	"io"
	"regexp"

	"gopkg.in/yaml.v3"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
)

// PodRegex compilation for parsing Podfile.lock package name and version
var podRegex = regexp.MustCompile(`([\w-/]+) \(([\d.]+)\)`)

// PodfileLock represents the structure of Podfile.lock
type podfileLock struct {
	Packages []parser.Package `yaml:"PODS"`
}

// AddPod adds a new pod to the podfileLock.
func (p *podfileLock) addPod(podData string) {
	newPod, err := parsePodData(podData)
	if err != nil {
		return
	}
	p.Packages = append(p.Packages, newPod)
}

// ParsePodData parses the given pod data string and returns a pod struct.
func parsePodData(podData string) (parser.Package, error) {
	matches := podRegex.FindStringSubmatch(podData)
	if len(matches) < 3 {
		return parser.Package{}, fmt.Errorf("invalid pod format")
	}

	return parser.Package{
		Name:    matches[1],
		Version: matches[2],
	}, nil
}

// UnmarshalYAML custom unmarshal method for Podfile.lock
func (p *podfileLock) UnmarshalYAML(unmarshal func(any) error) error {
	var raw map[string]any
	if err := unmarshal(&raw); err != nil {
		return err
	}

	pods, ok := raw["PODS"].([]any)
	if !ok {
		return fmt.Errorf("invalid PODS format")
	}

	for _, pod := range pods {
		switch pod := pod.(type) {
		case string:
			p.addPod(pod)
		case map[string]any:
			for key := range pod {
				p.addPod(key)
				// We can ignore `deps` as we're only interested in the first line
				break
			}
		}
	}

	return nil
}

// Parse extract the package name and version from Podfile.lock file
func Parse(r io.Reader, _ parser.Options) ([]parser.Package, []parser.Dependency, error) {
	var podFileLock podfileLock
	if err := yaml.NewDecoder(r).Decode(&podFileLock); err != nil {
		return nil, nil, err
	}

	return podFileLock.Packages, nil, nil
}

func init() {
	parser.Register("cocoapods", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypeCocoapods,
		Filenames:   []string{"Podfile.lock"},
	})
}
