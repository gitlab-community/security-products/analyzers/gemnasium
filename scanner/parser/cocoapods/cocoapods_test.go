package cocoapods

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/testutil"
)

func TestParseCocoapods(t *testing.T) {
	testCases := []struct {
		fixturePath string
		lockfile    string
	}{
		{
			fixturePath: "default",
			lockfile:    "Podfile.lock",
		},
		{
			fixturePath: "malformed",
			lockfile:    "Podfile.lock",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.lockfile, func(t *testing.T) {
			fixture := testutil.Fixture(t, tc.fixturePath, tc.lockfile)
			dependencies, _, err := Parse(fixture, parser.Options{})
			require.NoError(t, err)

			testutil.RequireExpectedPackages(t, tc.fixturePath, dependencies)
		})
	}
}
