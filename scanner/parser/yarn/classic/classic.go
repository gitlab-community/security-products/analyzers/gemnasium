package classic

import (
	"bufio"
	"fmt"
	"io"
	"regexp"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
)

const (
	yarnV1LockFileVersion = "1" // yarnV1LockFileVersion refers to Yarn v1
)

var (
	regexClassicFormatVersion = regexp.MustCompile(`\# yarn lockfile v(\d)`)
	regexClassicSpecList      = regexp.MustCompile(`^(\S.*):$`)
	regexSpec                 = regexp.MustCompile(`"?(@?[^@]+)@([^"]*)"?`)
	regexClassicVersion       = regexp.MustCompile(`^  version "(.*)"$`)
	regexClassicDependency    = regexp.MustCompile(`^    "?([^" ]+)"? "?([^"]*)"?$`)
)

// Definition combines a set of specs found at a given line
// with a package version and its dependencies
type Definition struct {
	Line         int64
	Specs        []Spec
	Version      string
	Dependencies []Spec
}

// Name is the package name
func (d Definition) Name() string {
	if len(d.Specs) == 0 {
		return ""
	}
	return d.Specs[0].Name
}

// ToSpec converts the package name and version to a spec
func (d Definition) ToSpec() Spec {
	return Spec{Name: d.Name(), Requirement: d.Version}
}

func (d Definition) String() string {
	return fmt.Sprintf("%s@%s", d.Name(), d.Version)
}

// Spec combines a required package with a required version range (requirement)
type Spec struct {
	Name        string
	Requirement string
}

func (s Spec) String() string {
	return fmt.Sprintf("%s@%s", s.Name, s.Requirement)
}

// Parse parses a classic yarn lock file and returns a list of packages
func Parse(r io.Reader) ([]parser.Package, []parser.Dependency, error) {
	// extract definitions
	defs, err := parseDefs(r)
	if err != nil {
		return nil, nil, err
	}

	// map that keeps track of root specs;
	// these appear in definitions but not in dependencies
	specIsRoot := map[Spec]bool{}

	// list packages and create package map
	pkgMap := map[Spec]*parser.Package{}
	pkgs := []parser.Package{}
	for _, def := range defs {
		// append package
		pkg := parser.Package{Name: def.Name(), Version: def.Version}
		pkgs = append(pkgs, pkg)

		// update package map
		specs := append(def.Specs, def.ToSpec())
		for _, spec := range specs {
			pkgMap[spec] = &pkg
		}

		// mark specs as orphan for now
		for _, spec := range def.Specs {
			specIsRoot[spec] = true
		}
	}

	// list dependencies
	deps := []parser.Dependency{}
	for _, def := range defs {
		fromSpec := def.ToSpec()
		fromPkg, _ := pkgMap[fromSpec]
		for _, toSpec := range def.Dependencies {
			// resolve dependency to package
			toPkg, ok := pkgMap[toSpec]
			if !ok {
				// dependency cannot be resolved
				return nil, nil, fmt.Errorf("cannot find yarn dependency %s required by %s", toSpec, fromSpec)
			}

			// spec is not a root spec
			delete(specIsRoot, toSpec)

			// append dependency
			deps = append(deps, parser.Dependency{
				From:         fromPkg,
				To:           toPkg,
				VersionRange: toSpec.Requirement,
			})
		}
	}

	// add dependencies for root specs
	for spec := range specIsRoot {
		pkg, _ := pkgMap[spec]
		deps = append(deps, parser.Dependency{
			To:           pkg,
			VersionRange: spec.Requirement,
		})
	}

	return pkgs, deps, nil
}

func parseDefs(r io.Reader) ([]Definition, error) {
	depfile := bufio.NewScanner(r)

	var line string
	var defs []Definition
	var lineNumber int64

	for depfile.Scan() {
		line = depfile.Text()
		lineNumber++

		switch {

		// empty line
		case len(line) == 0:
			continue

		// comment with format version (no indentation). The version provided
		// in this comment specifies the version for the entire lock file, and
		// should appear only once near the beginning of the file.
		case regexClassicFormatVersion.MatchString(line):
			matches := regexClassicFormatVersion.FindStringSubmatch(line)
			if formatVersion := matches[1]; formatVersion != yarnV1LockFileVersion {
				return nil, fmt.Errorf("invalid yarn lockfile version: %s", formatVersion)
			}

		// specs (no indentation)
		case regexClassicSpecList.MatchString(line):
			def, err := processSpec(line, lineNumber, depfile)
			if err != nil {
				return nil, err
			}
			defs = append(defs, *def)
		}
	}

	return defs, nil
}

func processSpec(spec string, lineNumber int64, depfile *bufio.Scanner) (*Definition, error) {
	def := &Definition{
		Line:         lineNumber,
		Dependencies: []Spec{},
	}
	matches := regexClassicSpecList.FindStringSubmatch(spec)
	rawSpecs := strings.Split(matches[1], ", ")
	def.Specs = make([]Spec, len(rawSpecs))
	for i, rawSpec := range rawSpecs {
		specMatch := regexSpec.FindStringSubmatch(rawSpec)
		if specMatch == nil {
			return nil, fmt.Errorf("invalid spec: %s", rawSpec)
		}
		def.Specs[i] = Spec{Name: specMatch[1], Requirement: specMatch[2]}
	}

	processingDependencies := false
	for depfile.Scan() {
		line := depfile.Text()

		if len(strings.TrimSpace(line)) == 0 {
			// once we encounter a blank line, we've finished processing dependencies and can return
			return def, nil
		}

		if line == "  dependencies:" || line == "  optionalDependencies:" {
			processingDependencies = true
			continue
		}

		if regexClassicVersion.MatchString(line) {
			matches := regexClassicVersion.FindStringSubmatch(line)
			def.Version = matches[1]
			continue
		}

		if processingDependencies {
			matches := regexClassicDependency.FindStringSubmatch(line)

			if len(matches) == 0 {
				return nil, fmt.Errorf("cannot parse dependency: %s", line)
			}

			spec := Spec{Name: matches[1], Requirement: matches[2]}
			def.Dependencies = append(def.Dependencies, spec)
		}
	}

	return def, nil
}

// IsValidLockfile parses the begining of a yarn.lock file and specifies if the yarn version used is in classic format
func IsValidLockfile(r io.Reader) (bool, error) {
	depfile := bufio.NewScanner(r)

	// We dont care about the first line
	depfile.Scan()
	depfile.Text()
	// Second line should tell us if this is yarn Classic
	depfile.Scan()
	line := depfile.Text()

	matches := regexClassicFormatVersion.FindStringSubmatch(line)
	if len(matches) == 0 {
		return false, nil
	}
	if formatVersion := matches[1]; formatVersion != yarnV1LockFileVersion {
		return false, fmt.Errorf("invalid yarn lockfile version: %s", formatVersion)
	}
	return true, nil
}
