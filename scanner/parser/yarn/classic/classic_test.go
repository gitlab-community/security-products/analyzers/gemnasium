package classic

import (
	"fmt"
	"regexp"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/testutil"
)

func TestYarnClassicRegex(t *testing.T) {
	t.Run("regexps", func(t *testing.T) {
		tcs := []struct {
			name   string
			Regexp *regexp.Regexp
			input  string
			want   []string
		}{
			{
				"regexFormatVersion",
				regexClassicFormatVersion,
				`# yarn lockfile v1`,
				[]string{"1"},
			},
			{
				"regexSpecList",
				regexClassicSpecList,
				`"mime-db@>= 1.24.0 < 2", mime-db@~1.25.0, mine-db@1.25.0:`,
				[]string{`"mime-db@>= 1.24.0 < 2", mime-db@~1.25.0, mine-db@1.25.0`},
			},
			{
				"regexSpec",
				regexSpec,
				`"mime-db@>= 1.24.0 < 2"`,
				[]string{"mime-db", ">= 1.24.0 < 2"},
			},
			{
				"regexVersion",
				regexClassicVersion,
				`  version "4.0.0"`,
				[]string{"4.0.0"},
			},
			{
				"regexDependency",
				regexClassicDependency,
				`    normalize-path "^2.0.1"`,
				[]string{"normalize-path", "^2.0.1"},
			},
			{
				"regexDependency escaped name",
				regexClassicDependency,
				`    "@nuxtjs/cssnano" "1.2.3"`,
				[]string{"@nuxtjs/cssnano", "1.2.3"},
			},
			{
				"regexDependency latest",
				regexClassicDependency,
				`    normalize-path latest`,
				[]string{"normalize-path", "latest"},
			},
			{
				"regexDependency conjunction",
				regexClassicDependency,
				`    mime-db ">= 1.40.0 < 2"`,
				[]string{"mime-db", ">= 1.40.0 < 2"},
			},
			{
				"regexDependency disjunction",
				regexClassicDependency,
				`    js-tokens "^3.0.0 || ^4.0.0"`,
				[]string{"js-tokens", "^3.0.0 || ^4.0.0"},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				all := tc.Regexp.FindStringSubmatch(tc.input)

				require.GreaterOrEqualf(t, len(all), 2, "Expected regexp to capture at least one string in %s", tc.input)
				got := all[1:]

				require.ElementsMatch(t, tc.want, got, "Expected regexp to capture\n%#v\nbut got\n%#v", tc.want, got)
			})
		}
	})
}

func TestIsValidLockfile(t *testing.T) {
	t.Run("regexps", func(t *testing.T) {
		tcs := []struct {
			name   string
			path   string
			want   bool
			errExp error
		}{
			{
				"file follows v1 structure but the version number is wrong",
				"../fixtures/classic/malformed/wrong-version",
				false,
				fmt.Errorf("invalid yarn lockfile version: 5"),
			},
			{
				"returns v1",
				"../fixtures/classic/simple",
				true,
				nil,
			},
			{
				"returns v2",
				"../fixtures/berry/v2/simple",
				false,
				nil,
			},
		}

		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				fixture := testutil.NewReader(t, tc.path, "yarn.lock")
				actual, err := IsValidLockfile(fixture)
				if tc.errExp != nil {
					require.EqualError(t, err, tc.errExp.Error())
				} else {
					require.NoError(t, err)
				}

				require.EqualValues(t, tc.want, actual)
			})
		}
	})
}
