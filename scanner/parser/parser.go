package parser

import (
	"errors"
	"io"
	"strings"
)

// ParseFunc scans a lock file and returns the project dependencies
// as a list of packages. It might also return a list of dependencies
// if the lock file describes what these packages require.
type ParseFunc func(io.Reader, Options) ([]Package, []Dependency, error)

// Options defines the available parser options.
type Options struct {
	// IncludeDev determines if devDependencies are included while parsing.
	IncludeDev bool
	// GradleResolutionPolicy defines the policy for handling Gradle dependency resolution states.
	GradleResolutionPolicy string
}

// PackageType is the package type
type PackageType string

const (
	// PackageTypeConan is the type of Conan packages
	PackageTypeConan = "conan"
	// PackageTypeGem is the type of RubyGems
	PackageTypeGem = "gem"
	// PackageTypeMaven is the type of Maven artifacts (Java, Scala)
	PackageTypeMaven = "maven"
	// PackageTypeNpm is the type of npm packages
	PackageTypeNpm = "npm"
	// PackageTypeNuget is the type of NuGet packages
	PackageTypeNuget = "nuget"
	// PackageTypePackagist is the type of PHP Composer packages
	PackageTypePackagist = "packagist"
	// PackageTypePypi is the type of Python packages
	PackageTypePypi = "pypi"
	// PackageTypeGo is the type of Go packages
	PackageTypeGo = "go"
	// PackageTypeCargo is the type for Cargo packages
	PackageTypeCargo = "cargo"
	// PackageTypeSwift is the type of Swift packages
	PackageTypeSwift = "swift"
	// PackageTypeCocoapods is the type of Cocoapods packages
	PackageTypeCocoapods = "cocoapods"
)

// Parser defines a parser capable of parsing a lock file
type Parser struct {
	Parse       ParseFunc   // Parse extracts package names and versions from a lock file
	PackageType PackageType // PackageType is the type of the packages listed by the parser
	Filenames   []string    // Filenames are the alternative names of the lock file
}

// Package describes a package version a project depends on
type Package struct {
	Name    string `json:"name"`    // Package name
	Version string `json:"version"` // Installed version
}

// ResolvedName resolves the name of the package if it is
// a local package. For example, the package name `../pkgA`
// will resolve to `pkgA`.
func (p Package) ResolvedName() string {
	return strings.TrimLeftFunc(p.Name, func(r rune) bool {
		return r == '.' || r == '\\' || r == '/'
	})
}

func (p Package) String() string {
	return p.ResolvedName() + " " + p.Version
}

// Dependency describes a dependency link between packages (transient dependencies)
// or between packages and the project itself (top-level dependencies)
type Dependency struct {
	// From is the package having a dependency.
	// This is set only in the case of transient dependencies,
	// and is null when this is a direct dependency of the project.
	From *Package `json:"from,omitempty"`

	// To is the required package
	To *Package `json:"to"`

	// VersionRange is the range of accepted versions for the dependency.
	// The version range syntax is specific to the lock file.
	VersionRange string `json:"range"`

	// Dev is true if the dependency is only needed during development,
	// and not at run-time. It defaults to false when this information
	// is not available in the lock file.
	Dev bool `json:"dev,omitempty"`
}

// Direct describes whether this is a direct dependency of the project
func (d Dependency) Direct() bool {
	return d.From == nil
}

var (
	// ErrWrongFileFormatVersion defines file format version errors
	ErrWrongFileFormatVersion = errors.New("wrong file format version")
	// ErrWrongFileFormat defines file format errors
	ErrWrongFileFormat = errors.New("wrong file format")
)
