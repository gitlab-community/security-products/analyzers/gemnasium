package nuget

import (
	"encoding/json"
	"io"
	"sort"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
)

// Document is a NuGet lock file
type Document struct {
	// Version is the format version
	Version int

	// Dependencies maps the .NET build targets to their dependencies
	Dependencies map[string]TargetDependencies
}

// TargetDependencies are all the dependencies of a .NET build target.
// This includes both direct dependencies and transitive dependencies.
type TargetDependencies map[string]DependencyInfo

// DependencyInfo describes a NuGet dependency
type DependencyInfo struct {
	// Type tells whether this is a direct project dependency.
	// It is either "direct" or "transitive".
	Type string

	// Requested is the requested version range.
	// It is set only for direct dependencies.
	Requested string

	// Resolved is the resolved version
	Resolved string

	// Dependencies is a list of transitive dependencies. It maps a package name
	// to a requested version range if the dependent package is a direct project dependency
	// (type is direct), and an exact version otherwise (type is transitive).
	Dependencies map[string]string
}

type nodeType string

const (
	nodeTypeProject nodeType = "Project"
	nodeTypeDirect  nodeType = "Direct"
)

// Returns whether the given lockfiel version can be parsed
func isSupportedVersion(version int) error {
	supportedFileFormatVersion := []int{1, 2}
	for _, supportedVersion := range supportedFileFormatVersion {
		if version == supportedVersion {
			return nil
		}
	}

	return parser.ErrWrongFileFormatVersion
}

// Parse scans a NuGet lock file and returns a list of packages
// It is not safe for concurrent use
func Parse(r io.Reader, _ parser.Options) ([]parser.Package, []parser.Dependency, error) {
	// decode lock file
	document := Document{}
	err := json.NewDecoder(r).Decode(&document)
	if err != nil {
		return nil, nil, err
	}

	// check format version
	if err := isSupportedVersion(document.Version); err != nil {
		return nil, nil, err
	}

	// collect targets like ".NETCoreApp,Version=v5.0"
	targets := []string{}
	for target := range document.Dependencies {
		targets = append(targets, target)
	}

	// sort targets to get a deterministic result when resolving dependencies;
	// targets are sorted in reverse order so that for instance dependencies defined in
	// ".NETCoreApp,Version=v5.0" win over the ones defined in ".NETCoreApp,Version=v3.0"
	sort.Sort(sort.Reverse(sort.StringSlice(targets)))

	// new graph to track packages and their dependencies
	g := newGraph()

	// add all the packages present in the lockfile to the graph
	for _, target := range targets {
		for name, info := range document.Dependencies[target] {
			pkg := parser.Package{Name: name, Version: info.Resolved}
			g.add(pkg, nodeType(info.Type), info.Requested)
		}
	}

	// add edges between nodes based on a package's dependencies attribute
	for _, target := range targets {
		for name, info := range document.Dependencies[target] {
			from, err := g.findByVersion(name, info.Resolved)

			if err != nil {
				return nil, nil, err
			}

			for dependencyName, requestedVersion := range info.Dependencies {
				// find first instance of node by its name
				to, err := g.findByName(dependencyName)

				if err != nil {
					return nil, nil, err
				}

				// exclude dependencies of Project type from the graph
				if to.nodeType == nodeTypeProject {
					continue
				}

				// a Project node's dependencies are changed to be Direct (from Transitive)
				// the requested version is also changed to the way Direct requested versions are retrieved
				if from.nodeType == nodeTypeProject {
					to.nodeType = nodeTypeDirect
					to.requested = to.pkg.Version
					continue
				}

				g.addEdge(from, to, requestedVersion)
			}
		}
	}

	return g.packages(), g.dependencies(), nil
}

func init() {
	parser.Register("nuget", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypeNuget,
		Filenames:   []string{"packages.lock.json"},
	})
}
