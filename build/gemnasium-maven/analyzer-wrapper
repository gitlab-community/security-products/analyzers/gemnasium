#!/bin/bash -l

set -euo pipefail

# The following two lines have been added as a workaround when gemnasium-maven is
# used in Docker-in-Docker mode. The workaround is necessary because the orchestrator
# layer from the common package doesn't propagate the environment variables from the
# Dockerfile, nor does it execute the .bashrc script when the container is started.
export ASDF_DATA_DIR="/opt/asdf"
. $HOME/.bashrc

export CI_DEBUG_TRACE=${CI_DEBUG_TRACE:='false'}
export HISTFILESIZE=0
export HISTSIZE=0
export LANG=C.UTF-8

[[ $CI_DEBUG_TRACE == 'true' ]] && echo "$@"

function debug_env() {
  pwd
  ls -alh
  env | sort
  asdf current

  java -version
}

switch_to_java "${DS_JAVA_VERSION:-17}"

# Different versions of Java require different versions of Gradle. We pre-install certain versions of
# gradle from the `.tool-versions` file, so we can switch to one of these supported gradle versions based
# on the version of Java provided in the DS_JAVA_VERSION variable. This allows scanning projects that
# don't use a gradle wrapper, and would otherwise cause an error because they're not locked to a specific
# version of gradle.
# See https://docs.gradle.org/current/userguide/compatibility.html#java for details.
#
# We can't just switch to Gradle 7.3 for all versions of Java, because there are
# breaking changes between Gradle version 6 and 7, for example.
# See https://docs.gradle.org/current/userguide/upgrading_version_6.html#sec:configuration_removal for details.
#
# If the DS_JAVA_VERSION variable is not defined, we use the first version specified in the ~/.tool-versions
# file as the default
case ${DS_JAVA_VERSION:-} in
    21)
        switch_to gradle "8.8"
        ;;
    16|17)
        switch_to gradle "7.6"
        ;;
    8|11|13|14|15)
        switch_to gradle "6.7"
        ;;
    *) ;;
esac

[[ $CI_DEBUG_TRACE == 'true' ]] && debug_env

/analyzer-binary "$@"
