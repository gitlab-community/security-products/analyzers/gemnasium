# UBI base and builder images
ARG UBI_IMAGE_VERSION=9.5
ARG UBI_IMAGE=registry.access.redhat.com/ubi9/ubi:${UBI_IMAGE_VERSION}
ARG UBI_IMAGE_MICRO=registry.access.redhat.com/ubi9/ubi-micro:${UBI_IMAGE_VERSION}

FROM registry.gitlab.com/gitlab-org/gitlab-runner/go-fips:1.22.7 AS analyzer-build

ENV CGO_ENABLED=0

WORKDIR /go/src/app
COPY go.mod go.sum ./
RUN go mod download

COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go version && \
    go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer \
    cmd/gemnasium/main.go

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS vrange-nuget-build

COPY vrange/nuget/Vrange /app
WORKDIR /app

RUN \
    dotnet restore && \
    dotnet build && \
    dotnet publish -c Release -r linux-x64 --self-contained true -p:PublishSingleFile=true -p:PublishTrimmed=true && \
    cp bin/Release/netcoreapp3.1/linux-x64/publish/Vrange /vrange-linux

FROM ${UBI_IMAGE_MICRO} as initial

FROM ${UBI_IMAGE} as build
USER root

# Runtime versions
ARG UBI_NGINX_VERSION=1.22
ARG UBI_NODEJS_VERSION=18
ARG UBI_PHP_VERSION=8.2
ARG UBI_RUBY_VERSION=3.1

ARG DNF_INSTALL_ROOT=/install-root
ARG DNF_OPTS_ROOT="--installroot=${DNF_INSTALL_ROOT}/ --setopt=reposdir=${DNF_INSTALL_ROOT}/etc/yum.repos.d/ --setopt=varsdir=/install-var/ --config=${DNF_INSTALL_ROOT}/etc/yum.repos.d/ubi.repo --setopt=cachedir=/install-cache/ --noplugins --releasever=${UBI_IMAGE_VERSION} -y"

ARG GEMNASIUM_DB_REF_NAME="master"
ARG GEMNASIUM_DB_REMOTE_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db.git"
ARG GEMNASIUM_DB_LOCAL_PATH="${DNF_INSTALL_ROOT}/gemnasium-db"

ARG RETIREJS_JS_ADVISORY_DB="${DNF_INSTALL_ROOT}/jsrepository-v4.json"

ARG RETIREJS_JS_ADVISORY_DB_URL="https://raw.githubusercontent.com/RetireJS/retire.js/master/repository/jsrepository-v4.json"

# See https://getcomposer.org/download/ for available versions of PHP Composer and corresponding checksums.
ARG COMPOSER_VERSION="2.4.1"
ARG COMPOSER_SHA256SUM="ea8cf6308ec76ff9645c3818841a7588096b9dc2767345fbd4bd492dd8a6dca6"
ARG COMPOSER_BIN="${DNF_INSTALL_ROOT}/usr/local/bin/composer"
ARG VRANGE_DIR="/vrange"

RUN mkdir -p ${DNF_INSTALL_ROOT}/ /install-var
COPY --from=initial / ${DNF_INSTALL_ROOT}/
COPY --from=analyzer-build /analyzer ${DNF_INSTALL_ROOT}/analyzer
COPY vrange ${DNF_INSTALL_ROOT}${VRANGE_DIR}
COPY --from=vrange-nuget-build /vrange-linux ${DNF_INSTALL_ROOT}${VRANGE_DIR}/nuget/vrange-linux

RUN \
    # ensure we have the most recent versions of all installed packages
    dnf ${DNF_OPTS_ROOT} update --nodocs && \
    # WARNING: The ubi8-minimal images purposefully do not include the timezone
    # data inside of /usr/share/zoneinfo to save space. This causes
    # composer to exit with a fatal error in PHP7, and segfault in PHP8.
    # This is because composer assumes that the timezone database exists.
    # To fix the fatal error / segfault issue, it's required to force a
    # re-install of tzdata and libksba which is also required by composer.
    # See https://bugzilla.redhat.com/show_bug.cgi?id=1903219 for more info.
    dnf ${DNF_OPTS_ROOT} install tzdata libksba

# install required dependencies
RUN dnf ${DNF_OPTS_ROOT} module enable nodejs:${UBI_NODEJS_VERSION} && \
    dnf ${DNF_OPTS_ROOT} install nodejs git curl --nodocs

# enable latest version of nginx with security updates
RUN dnf ${DNF_OPTS_ROOT} module reset nginx && \
    dnf ${DNF_OPTS_ROOT} module enable nginx:${UBI_NGINX_VERSION} && \
    dnf ${DNF_OPTS_ROOT} install nginx-filesystem --nodocs

# install git in the builder image to run the following commands
RUN dnf install -y git --nodocs

RUN \
    # gemnasium-db
    git clone --branch $GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_LOCAL_PATH && \
    # give write access to vulnerability database (OpenShift)
    chmod -R g+w $GEMNASIUM_DB_LOCAL_PATH

# vrange/php dependencies
RUN dnf ${DNF_OPTS_ROOT} module enable php:${UBI_PHP_VERSION} && \
    dnf ${DNF_OPTS_ROOT} install php php-common php-zip php-json php-xml php-mbstring --nodocs

ADD --checksum=sha256:$COMPOSER_SHA256SUM https://getcomposer.org/download/$COMPOSER_VERSION/composer.phar $COMPOSER_BIN

RUN chmod a+x $COMPOSER_BIN

# vrange/gem dependencies
RUN dnf ${DNF_OPTS_ROOT} module enable ruby:${UBI_RUBY_VERSION} && \
    dnf ${DNF_OPTS_ROOT} install ruby rubygem-json --nodocs

# vrange/nuget dependencies
RUN dnf ${DNF_OPTS_ROOT} install gettext-libs --nodocs

# give write access to CA certificates (OpenShift)
RUN mkdir -p ${DNF_INSTALL_ROOT}/etc/pki/ca-trust/source/anchors/ && \
    touch ${DNF_INSTALL_ROOT}/etc/pki/ca-trust/source/anchors/ca-certificates.crt && \
    chmod -R g+w ${DNF_INSTALL_ROOT}/etc/pki/

# download go toolchain required for determining the final list of modules used in a Go project
# via the golang.org/x/tools/go/packages package.
RUN dnf ${DNF_OPTS_ROOT} install go --nodocs

# clean microdnf cache
RUN dnf ${DNF_OPTS_ROOT} clean all

RUN curl -o $RETIREJS_JS_ADVISORY_DB $RETIREJS_JS_ADVISORY_DB_URL

FROM ${UBI_IMAGE_MICRO}
USER root
ARG DNF_INSTALL_ROOT=/install-root
COPY --from=build  ${DNF_INSTALL_ROOT}/ /

ARG RETIRE_JS_VERSION
ENV RETIRE_JS_VERSION ${RETIRE_JS_VERSION:-5.2.5}

# GEMNASIUM_RETIREJS_JS_ADVISORY_DB is used internally
# to communicate the local path of jsrepository.json to libfinder.
# The GEMNASIUM_ prefix prevents collisions with RETIREJS_JS_ADVISORY_DB
# of the retire.js analyzer.
ENV GEMNASIUM_RETIREJS_JS_ADVISORY_DB="/jsrepository-v4.json"

ENV VRANGE_DIR="/vrange"

ENV GEMNASIUM_DB_LOCAL_PATH="/gemnasium-db"
ENV GEMNASIUM_DB_REMOTE_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db.git"
ENV GEMNASIUM_DB_WEB_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db"
ENV GEMNASIUM_DB_REF_NAME="master"

# Necessary because of this behaviour with NodeJS >= 15: https://stackoverflow.com/a/65443098/17139068
WORKDIR /tmp

RUN \
    # update dependencies to latest version
    composer update -d "$VRANGE_DIR/php" && \
    composer install -d "$VRANGE_DIR/php" && \
    \
    # vrange/npm dependencies
    npm install -g yarn && \
    yarn --cwd "$VRANGE_DIR/npm/" && \
    \
    # install retire.js
    npm install -g retire@$RETIRE_JS_VERSION && \
    \
    # Enable OpenSSH to find relevant known_hosts files in the original home directory, and in the OpenShift compatible home directory we use.
    # See https://gitlab.com/gitlab-org/gitlab/-/issues/374571
    mkdir -p /root/.ssh && touch /root/.ssh/config && chmod 600 /root/.ssh/config && \
    echo 'UserKnownHostsFile /root/.ssh/known_hosts /tmp/.ssh/known_hosts' >> /root/.ssh/config && \
    echo "done"

# set user HOME to a directory where any user can write (OpenShift)
ENV HOME "/tmp"

ENTRYPOINT []
CMD ["/analyzer", "run"]
